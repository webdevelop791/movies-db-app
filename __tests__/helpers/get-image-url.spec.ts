import getImageUrl from "../../src/helpers/get-image-url";
import { IMAGE_HOST } from "../../src/config";

describe("should test getImageUrl", () => {
    test("and return valid value", () => {
        const name = "test.jpg";
        const width = 150;
        const expecting = `${IMAGE_HOST}w${width}/${name}`;

        expect(getImageUrl(name, width)).toBe(expecting);
    });
});
