export const MOVIES_LIST = "/movies";
export const MOVIE_VIEW = "/movies/:id";
export const HOME_PAGE = "/";
