import { HOME_PAGE, MOVIE_VIEW, MOVIES_LIST } from "./constants";
import * as React from "react";
import MoviesList from "../containers/movies/list";
import MovieView from "../containers/movies/view";
import Home from "../containers/home";

export interface RouteInterface {
    path: string;
    component: React.SFC<any> | React.ComponentClass<any>;
    exact?: boolean;
}

const routes: Array<RouteInterface> = [
    {
        path: HOME_PAGE,
        component: Home,
        exact: true
    },
    {
        path: MOVIES_LIST,
        component: MoviesList,
        exact: true
    },
    {
        path: MOVIE_VIEW,
        component: MovieView,
        exact: true
    }
];

export default routes;
