import * as React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PageWrap from "../components/wrap";
import routes, { RouteInterface } from "./routes";

class AppRoutes extends React.Component {
    renderRoutes = () => {
        return routes.map((route: RouteInterface) => (
            <Route key={route.path} path={route.path} component={route.component} exact />
        ));
    };

    render() {
        return (
            <Router>
                <PageWrap>
                    <Switch>{this.renderRoutes()}</Switch>
                </PageWrap>
            </Router>
        );
    }
}

export default AppRoutes;
