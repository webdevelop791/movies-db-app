import { all } from "redux-saga/effects";
import moviesSagas from "../containers/movies/list/sagas";
import movieSagas from "../containers/movies/view/sagas";

export default function* rootSagas() {
    yield all([moviesSagas(), movieSagas()]);
}
