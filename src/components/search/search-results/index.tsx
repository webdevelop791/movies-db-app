import * as React from "react";
import { Link } from "react-router-dom";
import * as classNames from "classnames/bind";
import styles from "../styles/index.less";
import { MovieInterface } from "../../../containers/movies/common/types";

interface Props {
    data: Array<MovieInterface>;
}

const cx = classNames.bind(styles);

const SearchResults = ({ data }: Props) => (
    <div className={cx("search-results")}>
        <ul>
            {data.map((item: MovieInterface) => (
                <li key={item.id}>
                    <Link to={`/movies/${item.id}`}>{item.original_title}</Link>
                </li>
            ))}
        </ul>
    </div>
);

export default SearchResults;
