interface Action {
    type: string;
}

export default function createReducer(initialState: Object, handlers: Object): Function {
    return function reducer(state: Object = initialState, action: Action): Object {
        if ({}.hasOwnProperty.call(handlers, action.type)) {
            return handlers[action.type](state, action);
        }
        return state;
    };
}
