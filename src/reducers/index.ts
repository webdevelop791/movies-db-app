import { combineReducers } from "redux";
import movies from "../containers/movies/list/reducer";
import movie from "../containers/movies/view/reducer";

export default combineReducers({ ...(movies as any), ...(movie as any) });
