import { MovieInterface } from "../../common/types";

export const MODULE_URL = "/movie/popular";

export interface FetchMoviesType {
    page: number;
}
export const FETCH_MOVIES = "/movies/fetch";
export const fetchMovies = (payload: FetchMoviesType) => ({
    type: FETCH_MOVIES,
    payload
});

export interface MoviesRequestResult {
    results: MovieInterface[];
    total_results: number;
    page: number;
}

export const FETCH_MOVIES_SUCCESS = "/movies/fetchSuccess";
export const fetchMoviesSuccess = (payload: MoviesRequestResult) => ({
    type: FETCH_MOVIES_SUCCESS,
    payload
});

export const FETCH_MOVIES_FAILURE = "/movies/fetchFailure";
export const fetchMoviesFailure = (payload: Object) => ({
    type: FETCH_MOVIES_FAILURE,
    payload
});

export interface SearchMoviesInterface {
    query: string;
}
export const SEARCH_MOVIES_URL = "/search/movie";
export const SEARCH_MOVIES = "/movies/search";
export const searchMovies = (payload: SearchMoviesInterface) => ({
    type: SEARCH_MOVIES,
    payload
});

export const SEARCH_MOVIES_SUCCESS = "/movies/searchSuccess";
export const searchMoviesSuccess = (payload: MoviesRequestResult) => ({
    type: SEARCH_MOVIES_SUCCESS,
    payload
});

export const SEARCH_MOVIES_FAILURE = "/movies/searchFailure";
export const searchMoviesFailure = (payload: Object) => ({
    type: SEARCH_MOVIES_FAILURE,
    payload
});
