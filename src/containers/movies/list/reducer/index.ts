import immutable from "immutability-helper";
import createReducer from "../../../../helpers/create-reducer";
import {
    FETCH_MOVIES,
    FETCH_MOVIES_SUCCESS,
    FETCH_MOVIES_FAILURE,
    SEARCH_MOVIES_SUCCESS,
    SEARCH_MOVIES_FAILURE
} from "../actions";
import { FETCH_MOVIE_SUCCESS, FETCH_MOVIE_FAILURE } from "../../view/actions";
import { MovieInterface } from "../../common/types";

interface InitialState {
    list: Array<MovieInterface>;
    search: {
        results: Array<MovieInterface>;
        loading: boolean;
        errors: boolean;
    };
    page: number;
    total_results: number;
    loading: boolean;
    errors: boolean;
}

const initialState = {
    list: [],
    search: {
        results: [],
        loading: false,
        errors: null
    },
    page: 1,
    total_results: 0,
    loading: false,
    errors: null
};

interface PayloadResult {
    payload: {
        results: Array<MovieInterface>;
        page: number;
        total_results: number;
    };
}

export default {
    movies: createReducer(initialState as InitialState, {
        [FETCH_MOVIES](state: InitialState) {
            return immutable(state, {
                loading: { $set: true }
            });
        },
        [FETCH_MOVIE_SUCCESS](state: InitialState) {
            return immutable(state, {
                search: {
                    results: { $set: [] }
                }
            });
        },
        [FETCH_MOVIE_FAILURE](state: InitialState) {
            return immutable(state, {
                search: {
                    results: { $set: [] }
                }
            });
        },
        [FETCH_MOVIES_SUCCESS](state: InitialState, { payload }: PayloadResult) {
            return immutable(state, {
                list: { $set: payload.results },
                search: {
                    results: { $set: [] }
                },
                page: { $set: payload.page },
                total_results: { $set: payload.total_results },
                loading: { $set: false }
            });
        },
        [FETCH_MOVIES_FAILURE](state: InitialState, { payload }: any) {
            return immutable(state, {
                errors: { $set: payload },
                loading: { $set: false }
            });
        },
        [SEARCH_MOVIES_SUCCESS](state: InitialState, { payload }: PayloadResult) {
            return immutable(state, {
                search: {
                    results: { $set: payload.results },
                    loading: { $set: false }
                }
            });
        },
        [SEARCH_MOVIES_FAILURE](state: InitialState, { payload }: any) {
            return immutable(state, {
                search: {
                    loading: { $set: false },
                    errors: { $set: payload }
                }
            });
        }
    })
};
