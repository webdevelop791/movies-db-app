import * as React from "react";
import { Row, Col, Pagination } from "antd";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import MovieCard from "./components/card";
import { fetchMovies } from "./actions";
import { MovieInterface } from "../common/types";

const { Fragment } = React;

interface MoviesListInterface {
    dispatch: Dispatch;
    movies: {
        list: Array<MovieInterface>;
        page: number;
        total_results: number;
    };
}

class MoviesList extends React.Component<MoviesListInterface> {
    componentDidMount() {
        this.changePageHandler(1);
    }

    changePageHandler = (page): void => {
        this.props.dispatch(fetchMovies({ page }));
    };

    render() {
        const { movies } = this.props;
        const { page, total_results } = movies;

        return (
            <div>
                <h1>Movies</h1>
                {movies.list.length ? (
                    <Fragment>
                        <Row gutter={{ lg: 20 }}>
                            {movies.list.map(movie => (
                                <Col span={6} key={movie.id}>
                                    <MovieCard movie={movie} />
                                </Col>
                            ))}
                        </Row>
                        <Pagination
                            current={page}
                            total={total_results}
                            pageSize={20}
                            onChange={this.changePageHandler}
                        />
                    </Fragment>
                ) : null}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    movies: state.movies
});

export default connect(mapStateToProps)(MoviesList);
