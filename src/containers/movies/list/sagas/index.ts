import { all, call, put, takeEvery } from "redux-saga/effects";
import {
    FETCH_MOVIES,
    SEARCH_MOVIES,
    SEARCH_MOVIES_URL,
    MODULE_URL,
    fetchMoviesSuccess,
    fetchMoviesFailure,
    searchMoviesSuccess,
    searchMoviesFailure
} from "../actions";
import api from "../../../../services/api";
import { FetchMoviesType, SearchMoviesInterface } from "../actions";

interface PayloadRequest {
    payload: FetchMoviesType;
}
function* fetchMoviesSaga({ payload }: PayloadRequest) {
    try {
        const list = yield call(api.fetch, MODULE_URL, payload);

        yield put(fetchMoviesSuccess(list));
    } catch (error) {
        yield put(fetchMoviesFailure(error));
    }
}

interface SearchPayloadRequest {
    payload: SearchMoviesInterface;
}
function* searchMoviesSaga({ payload }: SearchPayloadRequest) {
    try {
        const list = yield call(api.fetch, SEARCH_MOVIES_URL, payload);

        yield put(searchMoviesSuccess(list));
    } catch (error) {
        yield put(searchMoviesFailure(error));
    }
}

export default function* root() {
    yield all([
        takeEvery(FETCH_MOVIES as any, fetchMoviesSaga),
        takeEvery(SEARCH_MOVIES as any, searchMoviesSaga)
    ]);
}
