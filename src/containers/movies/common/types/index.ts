export interface Genre {
    id: number;
    name: string;
}

export interface MovieInterface {
    id: number;
    original_title: string;
    poster_path: string;
    vote_average: number;
    genres: Array<Genre>;
    overview: string;
}
