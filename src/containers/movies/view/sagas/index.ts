import { all, call, put, takeEvery } from "redux-saga/effects";
import {
    FETCH_MOVIE,
    MODULE_URL,
    fetchMovieSuccess,
    fetchMovieFailure,
    getMovieRecommendationsURL,
    getMovieSimilarURL
} from "../actions";
import api from "../../../../services/api";

interface FetchPayload {
    payload: number;
}

function* fetchMovieSaga({ payload }: FetchPayload) {
    try {
        const recommendationsURL = getMovieRecommendationsURL(payload);
        const recommendations = yield call(api.fetch, recommendationsURL);

        const similarURL = getMovieSimilarURL(payload);
        const similar = yield call(api.fetch, similarURL);

        const url = `${MODULE_URL}/${payload}`;
        const data = yield call(api.fetch, url);

        yield put(fetchMovieSuccess({ data, recommendations, similar }));
    } catch (error) {
        yield put(fetchMovieFailure(error));
    }
}

export default function* root() {
    yield all([takeEvery(FETCH_MOVIE as any, fetchMovieSaga)]);
}
