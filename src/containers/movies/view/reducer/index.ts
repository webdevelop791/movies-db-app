import immutable from "immutability-helper";
import createReducer from "../../../../helpers/create-reducer";
import { FETCH_MOVIE, FETCH_MOVIE_SUCCESS, FETCH_MOVIE_FAILURE } from "../actions";
import { MovieInterface } from "../../common/types";

interface StateInterface {
    data: Object;
    loading: boolean;
    errors: boolean;
    recommendations: Array<MovieInterface>;
    similar: Array<MovieInterface>;
}

interface SuccessPayload {
    payload: {
        data: MovieInterface;
        recommendations: {
            results: Array<MovieInterface>;
        };
        similar: {
            results: Array<MovieInterface>;
        };
    };
}

const initialState: StateInterface = {
    data: {},
    recommendations: [],
    similar: [],
    loading: false,
    errors: null
};

export default {
    movie: createReducer(initialState as StateInterface, {
        [FETCH_MOVIE](state: StateInterface) {
            return immutable(state, {
                loading: { $set: true }
            });
        },
        [FETCH_MOVIE_SUCCESS](state: StateInterface, { payload }: SuccessPayload) {
            return immutable(state, {
                data: { $set: payload.data },
                recommendations: { $set: payload.recommendations.results.slice(0, 10) },
                similar: { $set: payload.similar.results.slice(0, 10) },
                loading: { $set: false }
            });
        },
        [FETCH_MOVIE_FAILURE](state: StateInterface, { payload }: any) {
            return immutable(state, {
                errors: { $set: payload },
                loading: { $set: false }
            });
        }
    })
};
